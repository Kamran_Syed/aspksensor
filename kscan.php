#!/usr/bin/php
<?php
date_default_timezone_set('Asia/Karachi');

$db = getMeDbConn();

stream_set_blocking(STDIN, true);//Must be blocking

while(($line = fgetcsv(STDIN, 4096, "\t")) !== FALSE){

	$mac = $line[1];
	$newS = false;

	$r = getMac($mac);
	if(! $r){
		saveToDb($line);
		newSession($line);
		exec('/usr/bin/mpg321 -l 2 ./siren.mp3 >/dev/null 2>&1 &');
		$newS = true;
		
	}else{
	
		$cs = getSession($mac);

		if($cs !== false){

			updateSession($line, $r, $cs);

		}else{

			newSession($line, $r);
			$newS = true;
		}
	}

	updateDb($line, $r, $newS);

}








function getPastThirtyDays($mac){
	global $db;

	$chkdt = time()-2592000;

	$sql = $db->prepare("SELECT sum(tba) as a, sum(tbb) as b, sum(tbc) as c FROM hist where mac =? AND st > ?");
	$sql->execute(array($mac, $chkdt));
	$r = $sql->fetch(PDO::FETCH_ASSOC);

	if($r){
		$x = $r['a'].' - '.$r['b'].' - '.$r['c'];
	}else{
		$x = '';
	}

	return $x;
}

function calcAlertLeval($line, $r){
	//TODO
	return 1;
}

function getTimeBlock(){

	$tb = array('tba'=>0, 'tbb'=>0, 'tbc'=>0);
	$h = date('H');

	if($h >=0 && $h< 6){
		
		$tb['tba'] = 1;

	}elseif($h >=6 && $h< 18){
		$tb['tbb'] = 1;
	}else{
		$tb['tbc'] = 1;
	}

	return $tb;
}


function getMac($mac){
	global $db;

	$sql = $db->prepare("SELECT * FROM wiDb WHERE mac=?");
	$sql->execute(array($mac));
	$r = $sql->fetch(PDO::FETCH_ASSOC);
	return $r; //will be false if not found

}

function updateDb($line, $r, $newS){
	global $db;

	$dt = date('Y-m-d H:i:s');
	$dt2 = date('d H:i:s');
	$ts = time();

	//active since
	$act = null;
	$s = getSession($line[1]);
	if($s){
		$act = date('d H:i:s', $s['st']);
	}
	
	$lp = lastAndPreviousSeen($line[1]);
	$lst = $lp['last'];
	$lst2 = ($lp['last'] == null)? '' : date('H:i:s', strtotime($lst));
	$ps =  $lp['previous'];
	$ps2 = ($lp['previous'] == null)? '' : date('Y-m-d', strtotime($ps));

	$act =  $lp['act'];
	$act2 = ($lp['act'] == null)? '' : date('Y-m-d', strtotime($act));

	$firstseen = (! $r)? 'NEVER' : date('Y-m-d', strtotime($r['firstseen']));

	$tdays = getPastThirtyDays($line[1]);
	$alevel = calcAlertLeval($line, $r);
	$ssi = (string) $line[4];
	$avg = getAvgDuration($line[1]);

	$sql = $db->prepare("UPDATE wiDb SET tm=? , ap=?, activesince=?, lastseen=?, previouslyseen=?, seenperthirtydays=?, alertlevel=?, ssi=?, avgduration=? WHERE mac=?");
	$sql->execute(array($ts, $line[3], $act, $lst, $ps, $tdays, $alevel, $ssi, $avg['avgd'], $line[1]));

	if($newS & $r){
		echo $dt2."\t".$r['name']."\t".$line[1]."\t". $line[2]."\t". $line[3]."\t". $line[4]."\t LastToday=".$lst2."\t BeforeToday=".$ps2."\t First=".$firstseen."\t".$tdays.' avgd='.$avg['avgd']. ' Session Start'. PHP_EOL;
	}elseif($newS & !$r){
		echo $dt2."\t".$line[1]."\t". $line[2]."\t". $line[3]."\t". $line[4]."\t LastToday=NOW"."\t BeforeToday=NEVER"."\t First=NEVER"."\t".$tdays.' First Start'. PHP_EOL;
	}elseif(intval($ssi) > -70 && empty($r['name'])){
		echo $dt2."\t".$r['name']."\t".$line[1]."\t". $line[2]."\t". $line[3]."\t". $line[4]."\t LastToday=".$lst2."\t BeforeToday=".$ps2."\t First=".$firstseen."\t".$tdays.' avgd='.$avg['avgd']. ' ALERT'. PHP_EOL;
		//exec('/usr/bin/mpg321 -l 5 ./siren_pol_au.mp3 >/dev/null 2>&1 &');
	} 

}

function updateSession($line, $r, $cs){
	global $db;

	$dt = date('Y-m-d');
	$ts = time();
	$sl = $ts - $cs['st'];
	$ssi = (string) $line[4];
	$tb = getTimeBlock();

	$tba = ($tb['tba'] == 1)? 1 : $cs['tba'];
	$tbb = ($tb['tbb'] == 1)? 1 : $cs['tbb'];
	$tbc = ($tb['tbc'] == 1)? 1 : $cs['tbc'];

	$ap = (!empty($line[3]))? $line[3] : $cs['ap'];

	$sql = $db->prepare("UPDATE hist SET dt=?, ap=?, fn=? , sl=?, tba=?, tbb=?, tbc=?, ssi=? WHERE id=?");
	$sql->execute(array($dt, $ap, $ts, $sl, $tba, $tbb, $tbc, $ssi, $cs['id']));

}

function getSession($mac){
	global $db;

	$sql = $db->prepare("SELECT * FROM hist WHERE mac=? order by id desc limit 1 ");
	$sql->execute(array($mac));
	$r = $sql->fetch(PDO::FETCH_ASSOC);

	if($r){
		$diff = time() - $r['fn'];
		if($diff > 900) return false; //15min
	}

	return $r; //will be false if not found

}

function getAvgDuration($mac){
	global $db;

	$sql = $db->prepare("SELECT round(avg(sl)/60) as avgd, min(sl) as mind FROM hist where mac=? ");
	$sql->execute(array($mac));
	$r = $sql->fetch(PDO::FETCH_ASSOC);

	return $r; //will be false if not found

}

function lastAndPreviousSeen($mac){
	global $db;

	$sql = $db->prepare("SELECT * FROM hist WHERE mac=? order by id desc limit 2 ");
	$sql->execute(array($mac));
	$r = $sql->fetchAll(PDO::FETCH_ASSOC);

	$ret = array('last'=>null, 'previous'=>null, 'act'=>null);

	if(!empty($r[1])){

		$dt1 = date('Y-m-d', $r[1]['fn']);
		$dt2 = date('Y-m-d');

		if($dt1 != $dt2){
			$ret['previous'] = getPreviousSeen($mac);
		}else{
			$ret['previous'] = getPreviousSeen($mac);
			$ret['last'] = date('Y-m-d H:i:s', $r[1]['fn']);
			$ret['act'] = date('Y-m-d H:i:s', $r[0]['st']);
		} 

	}

	return $ret; //will be null if not found

}

function getPreviousSeen($mac){
	global $db;

	$dt2 = date('Y-m-d');

	$sql = $db->prepare("SELECT * FROM hist WHERE mac=? and dt !=? order by id desc ");
	$sql->execute(array($mac, $dt2));
	$r = $sql->fetch(PDO::FETCH_ASSOC);

	if($r){

		$ret = date('Y-m-d H:i:s', $r['fn']);

	}

	return $ret; //will be null if not found

}

function newSession($line, $r=array()){
	global $db;

	$dt = date('Y-m-d');
	$dt2 = date('d H:i:s');
	$ts = time();
	$ssi = (string) $line[4];
	$tb = getTimeBlock();

	$sql = $db->prepare("INSERT INTO hist SET dt=?, mac=?, ap=? , st=?, fn=?, sl=?, tba=?, tbb=?, tbc=?, ssi=? ");
	
	$sql->execute(array($dt, $line[1], $line[3], $ts, $ts, 0, $tb['tba'], $tb['tbb'], $tb['tbc'], $ssi ));

}

function saveToDb($line){
	global $db;

	$dt = date('Y-m-d H:i:s');
	echo $dt."\t".$line[1]."\t". $line[2]."\t". $line[3]."\t". $line[4]. '  ****NEW****'.PHP_EOL;
	$ts = time();
	$ssi = (string) $line[4];

	$sql = $db->prepare("INSERT INTO wiDb SET tm=?, mac=? , company=?, ap=?, activesince=?, lastseen=?, firstseen=?, previouslyseen=?, seenperthirtydays=?, alertlevel=?, ssi=? ");
	
	$sql->execute(array($ts, $line[1], $line[2], $line[3], $dt, $dt, $dt, $dt, 1, 9, $ssi ));

}


function getMeDbConn(){
	$host = "localhost";
	$username = "root";
	$password = "margalla99";
	$dbname = "aspkscanner";
	
	// Create connection
	$db = new PDO('mysql:host='.$host.';dbname='.$dbname.';charset=utf8mb4', $username, $password, array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
	
	// Check connection
	try{
		$x =$db->query("show tables;");
		$cnt = $x->rowCount();
		
	}catch(PDOException $ex) {
		echo $ex->getMessage();
		exit;
	}
	return $db;
}

//python probemon.py -i mon0 -t unix -o ~/WonderHowTo -f -s -r -l | ./kscan.php

?>